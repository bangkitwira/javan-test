import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { People } from 'src/app/models/People';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnChanges {
  userForm: FormGroup;

  userPayload: People = {
    firstName: '',
    lastName: '',
    address: '',
    job: '',
    salary: undefined,
  };
  @Output() onSubmit = new EventEmitter<People>();
  @Output() onUpdate = new EventEmitter<People>();
  @Input() isLoading = false;
  @Input() selectedUser: People | undefined = undefined;
  constructor(private fb: FormBuilder) {
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      job: ['', [Validators.required]],
      salary: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}

  submit() {
    if (this.userForm.invalid) {
      this.userForm.markAllAsTouched();
      return;
    }
    this.userPayload.firstName = this.userForm.controls['firstName'].value;
    this.userPayload.lastName = this.userForm.controls['lastName'].value;
    this.userPayload.address = this.userForm.controls['address'].value;
    this.userPayload.salary = this.userForm.controls['salary'].value;
    this.userPayload.job = this.userForm.controls['job'].value;
    this.userPayload?.id
      ? this.onUpdate.emit(this.userPayload)
      : this.onSubmit.emit(this.userPayload);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedUser'].currentValue) {
      console.log(changes['selectedUser'].currentValue);
      this.userPayload = changes['selectedUser'].currentValue;
      this.userForm.controls['firstName'].setValue(this.userPayload.firstName);
      this.userForm.controls['lastName'].setValue(this.userPayload.lastName);
      this.userForm.controls['address'].setValue(this.userPayload.address);
      this.userForm.controls['job'].setValue(this.userPayload.job);
      this.userForm.controls['salary'].setValue(this.userPayload.salary);
    }
  }
}
