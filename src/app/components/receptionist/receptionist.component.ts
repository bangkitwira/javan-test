import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { People } from 'src/app/models/People';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-receptionist',
  templateUrl: './receptionist.component.html',
  styleUrls: ['./receptionist.component.scss'],
})
export class ReceptionistComponent implements OnInit {
  peoples: People[] = [];
  @Output() onClickUser = new EventEmitter<People>();
  @Output() onRemoveUser = new EventEmitter<People['id']>();
  constructor(private contactService: ContactsService) {}

  ngOnInit(): void {
    this.getContacts();
  }

  async getContacts() {
    try {
      const response = await this.contactService.getContacts();
      if (response) this.peoples = response;
      console.log(this.peoples);
    } catch {}
  }

  onUserClick(user: People) {
    this.onClickUser.emit(user);
  }

  onRemoveUserClick(userId: People['id']) {
    this.onRemoveUser.emit(userId)
  }
}
