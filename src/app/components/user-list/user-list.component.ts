import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { People } from 'src/app/models/People';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent {

  @Input() peoples: People[] = []
  @Output() onClickUser = new EventEmitter<People>();
  @Output() onRemoveUser = new EventEmitter<People['id']>();
  constructor() {}

  onUserClick(user: People) {
    this.onClickUser.emit(user);
  }

  onRemoveUserClick($event: MouseEvent, userId: People['id']) {
    $event.stopPropagation()
    this.onRemoveUser.emit(userId);
  }
}
