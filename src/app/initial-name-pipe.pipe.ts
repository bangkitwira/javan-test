import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initialNamePipe'
})
export class InitialNamePipePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    return value.substring(0,1);
  }

}
