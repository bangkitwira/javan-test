import { Injectable } from '@angular/core';
import { People } from '../models/People';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class ContactsService {
  constructor(private apiService: ApiService) {}

  async getContacts() {
    const response = await this.apiService
      .httpGet('/contacts')
      .then((response: People[]) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
    return response;
  }

  async postUser(payload: People) {
    const response = await this.apiService
      .httpPost('/contacts', payload)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
    return response;
  }

  async putUser(id: string | undefined, payload: People) {
    const response = await this.apiService
      .httpPut('/contacts', id, payload)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
    return response;
  }

  async deleteUser(id: string | undefined) {
    const response = await this.apiService
      .httpDelete('/contacts', id)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
    return response;
  }
}
