export interface People {
  id?: string;
  createdAt?: string;
  firstName: string;
  lastName: string;
  address: string;
  job: string;
  salary: number | undefined
}
