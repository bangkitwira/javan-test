import { Component, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReceptionistComponent } from '../../components/receptionist/receptionist.component';
import { UserFormComponent } from '../../components/user-form/user-form.component';
import { People } from '../../models/People';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {
  @ViewChild(UserFormComponent) userFormComponent: any;
  @ViewChild(ReceptionistComponent) receptionistComponent: any;
  isLoading = false;
  selectedUser: People | undefined = undefined;
  constructor(
    private contactService: ContactsService,
    private snackBar: MatSnackBar
  ) {}

  onUserClick(user: People) {
    console.log(user);
    this.selectedUser = user;
  }

  async submitUser(people: People) {
    this.isLoading = true;
    try {
      await this.contactService.postUser(people);
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    } finally {
      this.snackBar.open('User successfully added', 'dismiss', {
        duration: 3000,
      });
      this.receptionistComponent.getContacts();
      this.userFormComponent.userForm.reset();
      this.userFormComponent.markAsUntouched();
    }
  }

  async updateUser(people: People) {
    this.isLoading = true;
    try {
      await this.contactService.putUser(people?.id, people);
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    } finally {
      this.snackBar.open('User successfully updated', 'dismiss', {
        duration: 3000,
      });
      this.receptionistComponent.getContacts();
    }
  }

  async deleteUser(userId: People['id']) {
    this.isLoading = true;
    try {
      await this.contactService.deleteUser(userId);
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    } finally {
      this.snackBar.open('User successfully deleted', 'dismiss', {
        duration: 3000,
      });
      this.receptionistComponent.getContacts();
    }
  }
}
