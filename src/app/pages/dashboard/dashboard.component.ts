import { Component, OnInit } from '@angular/core';
import { People } from 'src/app/models/People';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  chartBarData: People['salary'][] = [];
  chartBarCategories: People['firstName'][] = [];
  pieChartData: number[] = [];
  pieChartLabel: string[] = [];
  constructor(private contactService: ContactsService) {}

  ngOnInit(): void {
    this.getContacts();
  }

  async getContacts() {
    try {
      const response = await this.contactService.getContacts();
      if (response) {
        this.chartBarData = response.map((contact: People) => contact.salary);
        this.chartBarCategories = response.map(
          (contact: People) => contact.firstName
        );
        this.pieChartData = [
          response.filter((contact) => contact.job === 'designer').length,
          response.filter((contact) => contact.job === 'engineer').length,
          response.filter((contact) => contact.job === 'developer').length,
        ];
        this.pieChartLabel = ['designer', 'engineer', 'developer'];
      }
    } catch {}
  }
}
